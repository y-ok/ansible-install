# ansible-install

## `Provate.repo`の配備

```bash
[root@localhost ~]# cd /etc/yum.repos.d/
[root@localhost yum.repos.d]# mkdir bak
[root@localhost yum.repos.d]# mv CentOS-* bak/.
[root@localhost yum.repos.d]# mv epel* bak/.
[root@localhost yum.repos.d]# cp -pr [リポジトリパス]/Private.repo .
[root@localhost yum.repos.d]# ll
total 4
drwxr-xr-x. 2 root root 262 Jun  8 13:21 bak
-rw-r--r--. 1 root root 110 Jun  8 13:21 Private.repo
```

## `yum info ansible`確認

```bash
[root@localhost ~]# yum info ansible
Loaded plugins: fastestmirror
local-private-repository                                                                                                                                                                                                 | 2.5 kB  00:00:00     
local-private-repository/primary_db                                                                                                                                                                                      |  13 kB  00:00:00     
Loading mirror speeds from cached hostfile
Available Packages
Name        : ansible
Arch        : noarch
Version     : 2.9.9
Release     : 1.el7
Size        : 17 M
Repo        : local-private-repository
Summary     : SSH-based configuration management, deployment, and task execution system
URL         : http://ansible.com
License     : GPLv3+
Description : Ansible is a radically simple model-driven configuration management,
            : multi-node deployment, and remote task execution system. Ansible works
            : over SSH and does not require any software or daemons to be installed
            : on remote nodes. Extension modules can be written in any language and
            : are transferred to managed machines automatically.
```

## インストール

```bash
[root@localhost ~]# rpm -qa | grep ansible
[root@localhost ~]# yum -y install ansible
```

## インストール後確認

```bash
[root@localhost ~]# rpm -qa | grep ansible
ansible-2.9.9-1.el7.noarch
```
